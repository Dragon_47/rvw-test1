<?php


class Formatters
{
    /**
     * Provides consistent formatting for phone numbers.
     *
     * @param string $phone
     * @return string
     */
    public function FormatPhone($phone)
    {
		$PNum = explode('-', $phone);
		$PNum = implode('', $PNum);
		$PNum = explode(' ', $PNum);
		$PNum = implode('', $PNum);
		$PNum = explode('.', $PNum);
		$PNum = implode('', $PNum);
		return '(' . substr($PNum, 0, 3) . ') ' . substr($PNum, 3, 3) . substr($PNum, 5, 0) . '-' . substr($PNum, 6);
    }

    /**
     * Returns a singular or plural string, based on the quantity.
     *
     * @param string $singular_string
     * @param int|float $quantity
     * @return string
     */
    public function Pluralize($singular_string, $quantity)
    {
		
		if (is_int($quantity)){
			if ($quantity != 1){
				if($singular_string == 'Pony')
					return 'Ponies';
				else if($singular_string == 'pony')
					return 'ponies';
				return $singular_string . 's';
			}
			else{
				return $singular_string;
			}
		}
        // TODO: implement this method.
		return 'problem';
    }
}
