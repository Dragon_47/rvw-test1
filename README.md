# RV Wholesalers PHP Test 1

### Requires

- PHP >= 7.0

### Instructions

1. Fork this repository
2. Clone your fork
3. In the project root run this command:
    ```
    php composer.phar require phpunit/phpunit
    ```

4. Run the tests with this command, both tests should fail:
    ```
    php ./vendor/bin/phpunit
    ```

5. Debug Formatters::FormatPhone. Fix the function so the test passes.
6. Implement Formatters::Pluralize so the test passes.
7. Commit the changes and push them to your fork.
8. Create a pull request in this repository, requesting your changes be
   added into this repository.

### Links that may be helpful

<https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/>

<https://support.atlassian.com/bitbucket-cloud/docs/clone-a-repository/>
